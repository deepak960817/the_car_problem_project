// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. 
//Using the array you just obtained from the previous problem, 
//find out how many cars were made before the year 2000 and return the array of older cars and log its length.

function problem5(inventory, allCarYears) {
    if(typeof inventory === 'undefined' || inventory.length === 0)
    {
        return "No arguments passed or Empty inventory array is Passed";
    
    }
    if(typeof allCarYears === 'undefined' || allCarYears.length === 0)
    {
        return "No arguments passed or Empty inventory array is Passed";
    }
    if(typeof allCarYears !== 'object')
    {
        return "Arguments type incorrect";
    }
    let olderCars=0;
    let carYears = [];
    for(let i=0;i<allCarYears.length;i++)
    {
        if(allCarYears[i] > 2000)
        {
            carYears[olderCars++] = allCarYears[i];
        }
    }
    return carYears;
};

module.exports = problem5;