// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. 
//Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.


function problem4 (inventory) {
    
    if(typeof inventory === 'undefined' || inventory.length === 0)
    {
        return "No arguments passed or Empty inventory array is Passed";
    }
    if(typeof inventory !== 'object')
    {
        return "Arguments type incorrect";
    }

    let carYears = [];
    for(let i=0;i<inventory.length;i++)
    {
        carYears[i] = (inventory[i]["car_year"]);
    }
    return carYears;
};

module.exports = problem4;
