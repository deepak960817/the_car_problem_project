// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. 
//Execute a function to find what the make and model of the last car in the inventory is?  
//Log the make and model into the console in the format of: 
//"Last car is a *car make goes here* *car model goes here*"

function problem2(inventory) {

    if(typeof inventory === 'undefined')
    {
        return "No arguments passed";
    }
    if(inventory.length === 0)
    {
        return "Empty/No inventory array is Passed";
    }
    if(typeof inventory !== 'object')
    {
        return "Arguments type incorrect";
    }
    let lastCar;
    for(let i=inventory.length-1; i>=0; i--)
    {
        if(inventory[i]["id"] === inventory.length)
        {
            lastCar = inventory[i];
            break;
        }
    }
        return lastCar;
};

module.exports = problem2;