// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. 
//Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.


function problem3 (inventory) {
    
    if(typeof inventory === 'undefined')
    {
        return "No arguments passed/ Passed argument not defined";
    }
    if(inventory.length === 0)
    {
        return "Empty/No inventory array is Passed";
    }
    if(typeof inventory !== 'object')
    {
        return "Arguments type incorrect";
    }
    let carModels = [];
    for(let i=0;i<inventory.length;i++)
    {
        let model = inventory[i]["car_model"];
        carModels[i] = model.toUpperCase();
    }
    return carModels.sort();
};

module.exports = problem3;